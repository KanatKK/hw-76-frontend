import React from 'react';

const Messages = props => {
    return (
        <div className="messages">
            <h3>{props.author}</h3>
            <p>{props.message}</p>
            <p className="dateTime">{props.dateTime}</p>
        </div>
    );
};

export default Messages;