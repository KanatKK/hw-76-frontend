import React from 'react';

const Send = props => {
    return (
        <div className="send">
            <input type="text" value={props.author} className="author" placeholder="Author" onChange={props.addAuthor}/>
            <input type="text" value={props.message} className="message" placeholder="Message" onChange={props.addText}/>
            <button type="button" className="sendBtn" onClick={props.send}>Send</button>
        </div>
    );
};

export default Send;