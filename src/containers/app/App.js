import React, {useEffect} from 'react';
import './App.css';
import Messages from "../../components/messages/messages";
import Send from "../../components/send/send";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {addAuthor, addNewMessage, addText, fetchMessages} from "../../store/actions";

function App() {
    const dispatch = useDispatch();
    const messages = useSelector(state => state.get.messages);
    const text = useSelector(state => state.post.message);
    const author = useSelector(state => state.post.author);
    const postReducer = useSelector(state => state.post);

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch]);

    useEffect(() => {
        const interval = setInterval(async() => {
            const response = await axios.get('http://localhost:8000/messages');
            if (response.data.length > 0) {
                dispatch(fetchMessages());
            }
        }, 2000);
        return () => clearInterval(interval);
    }, [messages, dispatch]);

    let addTextHandler = event => {
        dispatch(addText(event.target.value));
    };

    let addAuthorHandler = event => {
        dispatch(addAuthor(event.target.value));
    };

    let sendMessage = async () => {
        await addNewMessage({...postReducer});
        dispatch(addText(''));
        dispatch(addAuthor(''));
    };


    if (messages) {
        let messagesList = messages.map((messages, index) => {
            return (
                <Messages
                    key={index} author={messages.author}
                    message={messages.message} dateTime={messages.datetime}
                />
            );
        });
        return (
            <div className="container">
                <Send
                    message={text} author={author}
                    addText={addTextHandler} addAuthor={addAuthorHandler}
                    send={sendMessage}
                />
                <div className="allMessages">
                    {messagesList}
                </div>
            </div>
        );
    } else {
        return null
    }
}

export default App;