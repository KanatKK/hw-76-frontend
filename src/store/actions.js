import {ADD_AUTHOR, ADD_TEXT, GET_MESSAGES} from "./actionTypes";
import axios from "axios";

export const getMessages = value => {
    return {type: GET_MESSAGES, value};
};

export const addText = value => {
    return {type: ADD_TEXT, value};
};
export const addAuthor = value => {
    return {type: ADD_AUTHOR, value};
};

export const fetchMessages = () => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/messages');
            dispatch(getMessages(response.data));
        } catch (e) {
            console.error();
        }
    };
};

export const addNewMessage = async value => {
    try {
        await axios.post('http://localhost:8000/messages', value);
    } catch (e) {
        console.error();
    }
};