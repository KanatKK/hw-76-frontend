import {ADD_AUTHOR, ADD_TEXT} from "../actionTypes";

const initialState = {
    message: '',
    author: '',
};

const postMessages = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TEXT:
            return {...state, message: action.value};
        case ADD_AUTHOR:
            return {...state, author: action.value};
        default:
            return state;
    }
};

export default postMessages;